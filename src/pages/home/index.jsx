import React, {Component} from 'react';
import { Row, Col, Carousel} from 'antd';
import {Divider} from 'antd';
import TopBar from 'components/TopBar';
import TopMenu from 'components/TopMenu'
import SearchBar from 'components/SearchBar'
import CategoryNavigator from 'components/CategoryNavigator';
import Floor from './Floor';

class Home extends Component {
    render() {
        return (
            <div>
                <Row>
                    <Col span={3}/>
                    <Col span={18}>
                        <TopBar/>
                    </Col>
                    <Col span={3}/>
                </Row>
                <Divider style={{margin: '0'}}/>
                <Row>
                    <Col span={3}/>
                    <SearchBar/>
                    <Col span={3}/>
                </Row>
                <Row>
                    <Col offset={3}>
                        <TopMenu/>
                    </Col>
                </Row>
                <Divider style={{margin: '0'}}/>
                <Row>
                    <Col span={5} offset={3}>
                        <CategoryNavigator/>
                    </Col>
                    <Col span={12}>
                        <Carousel autoplay>
                            <div><img style={{height:'400px'}} src='http://image.lingshi.com/data/afficheimg/1520582029696879186.jpg'/></div>
                            <div><img style={{height:'400px'}} src='http://image.lingshi.com/data/afficheimg/1517810945694990298.jpg'/></div>
                            <div><img style={{height:'400px'}} src='http://image.lingshi.com/data/afficheimg/1491805028600124816.jpg'/></div>
                            <div><img style={{height:'400px'}} src='http://image.lingshi.com/data/afficheimg/1520582827474133451.jpg'/></div>
                        </Carousel>
                    </Col>
                </Row>

                <Row>
                    <Col span={18} offset={3}>
                        <Floor/>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default Home;
