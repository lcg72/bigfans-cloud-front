/**
 * Created by lichong on 2/14/2018.
 */

import React, {Component} from 'react';
import {Icon, Menu} from 'antd';

import {Link} from 'react-router-dom'
import StorageUtils from 'utils/StorageUtils'
import HttpUtils from 'utils/HttpUtils'

class TopBar extends Component {

    state = {
        login : false,
        loginAccount : ''
    }

    constructor(props){
        super(props)
    }

    componentDidMount(){
        let loginAccount = StorageUtils.getTokenAccount();
        if(loginAccount){
            this.setState({login : true , loginAccount})
        } 
    }

    onLogout(){
        let self = this;
        HttpUtils.logout({
            success(resp){
                StorageUtils.removeTokenObject()
                self.location.href.push('/')
            }
        })
    }

    render() {
        return (
            <div>
                <div className="pull-left">
                    <Menu
                        mode="horizontal"
                    >
                        <Menu.Item key="home">
                            <Link to="/">首页</Link>
                        </Menu.Item>
                    </Menu>
                </div>
                <div className="pull-right">
                    <Menu
                        mode="horizontal"
                    >
                        {
                            this.state.login
                            ?
                            <Menu.Item key="mail">
                                <Link to="/center"><Icon type="mail"/>{this.state.loginAccount}</Link>
                            </Menu.Item>
                            :
                            <Menu.Item key="login">
                                <Link to="/login"><Icon type="appstore"/>登录</Link>
                            </Menu.Item>
                        }
                        <Menu.Item key="app">
                            <Link to="/myorders"><Icon type="appstore"/>我的订单</Link>
                        </Menu.Item>
                        <Menu.Item key="center">
                            <a href="/center"><Icon type="appstore"/>个人中心</a>
                        </Menu.Item>
                        {
                            this.state.login
                            &&
                            <Menu.Item key="login">
                                <a href="javascript:void(0)" onClick={() => this.onLogout()}><Icon type="appstore"/>退出</a>
                            </Menu.Item>
                        }
                    </Menu>
                </div>
            </div>
        );
    }
}

export default TopBar;