import React, { Component } from 'react';
import { Layout} from 'antd';
import Home from './pages/home'

import './App.css';

class App extends Component {
  render() {
    return (
      <div>
          <Layout className="App">
              <Home/>
          </Layout>
      </div>
    );
  }
}

export default App;
